package com.example.demo.controller;

import com.example.demo.dto.LoginDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityDto;
import com.example.demo.dto.UserDto;
import com.example.demo.service.ProductService;
import com.example.demo.service.ShoppingCartService;
import com.example.demo.service.UserService;
import com.example.demo.validator.ProductDtoValidator;
import com.example.demo.validator.UserDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class MainController {
    @Autowired
    private ProductDtoValidator productDtoValidator;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserDtoValidator userDtoValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @GetMapping(value = "/product")
    public String productPageGet(Model model, @ModelAttribute("addProductMessage") String addProductMessage) {
        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        model.addAttribute("addProductMessage", addProductMessage);
        return "addProduct";
    }

    @PostMapping(value = "/product")
    public String productPagePost(@ModelAttribute("productDto") ProductDto productDto, BindingResult bindingResult, RedirectAttributes redirectAttributes, @RequestParam("productImage") MultipartFile multipartFile) {
        productDtoValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }
        productService.addProduct(productDto, multipartFile);
        redirectAttributes.addFlashAttribute("addProductMessage", productDto.getName() + " was added successfully!");
        return "redirect:/product";
    }

    @GetMapping(value = "/home")
    public String viewProductsGet(Model model) {
        List<ProductDto> productDtoList = productService.getAllProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        return "home";
    }

    @GetMapping(value = "/product/{id}")
    public String viewProductGet(Model model, @PathVariable(value = "id") String id) {
        Optional<ProductDto> productDto = productService.getProductDtoById(id);
        if (productDto.isPresent()) {
            model.addAttribute("productDto", productDto.get());
            model.addAttribute("quantityDto", new QuantityDto());
            return "viewProduct";
        } else {
            return "error";
        }
    }

    @GetMapping(value = "/registration")
    public String registrationGet(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "registration";
    }

    @PostMapping(value = "/registration")
    public String registrationPost(@ModelAttribute(name = "userDto") UserDto userDto, Model model, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        userDtoValidator.validate(userDto, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("message", "Invalid input form");
            return "registration";
        }
        userService.addUser(userDto);
        redirectAttributes.addFlashAttribute("registerUser", "User with email " + userDto.getEmail() + " was registered successfully!");
        return "redirect:/login";
    }

    @GetMapping(value = "/login")
    public String loginGet(Model model) {
        LoginDto loginDto = new LoginDto();
        model.addAttribute("loginDto", loginDto);
        return "login";
    }

    @PostMapping(value = "/login")
    public String loginPost(@ModelAttribute(value = "loginDto") LoginDto loginDto, Model model) {
        boolean isLoginSuccessful = userService.login(loginDto);
        if (isLoginSuccessful) {
            return "redirect:/home";
        } else {
            model.addAttribute("message", "Sorry, invalid email or password!");
            return "login";
        }
    }

    @PostMapping(value = "/product/{productId}/add")
    public String addToCartPost(@PathVariable(value="productId") String productId, @ModelAttribute(name = "quantityDto") QuantityDto quantityDto, Authentication authentication){
        System.out.println("A dat comanda userul: " + authentication.getName() + " o cantitate de " + quantityDto.getQuantity());
        shoppingCartService.addProduct(productId, quantityDto, authentication.getName());
        return "redirect:/product/" + productId;
    }

    @GetMapping(value = "/shoppingCart")
    public String shoppingCartGet(Model model, Authentication authentication){
        Map<ProductDto, Integer> productMap = shoppingCartService.getShoppingCartContent(authentication.getName());
        model.addAttribute("productMap", productMap);
        return "shoppingCart";
    }
}
