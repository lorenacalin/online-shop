package com.example.demo.mapper;

import com.example.demo.dto.UserDto;
import com.example.demo.model.ShoppingCart;
import com.example.demo.model.User;
import com.example.demo.model.Wishlist;
import com.example.demo.model.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User mapUser(UserDto userDto){
        User user = new User();
        user.setName(userDto.getName());
        user.setAddress(userDto.getAddress());
        user.setEmail(userDto.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setRole(Role.valueOf(userDto.getRole()));

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUser(user);
        Wishlist wishlist = new Wishlist();
        wishlist.setUser(user);
        user.setShoppingCart(shoppingCart);
        user.setWishlist(wishlist);
        return user;
    }

    public UserDto mapUserDto(User user){
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());
        userDto.setAddress(user.getAddress());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setRole(user.getRole().toString());
        return userDto;
    }
}
