package com.example.demo.mapper;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.Product;
import com.example.demo.model.Stock;
import com.example.demo.model.enums.Category;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductMapper {
    public Product mapProduct(ProductDto productDto, MultipartFile multipartFile){
        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrice(Integer.valueOf(productDto.getPrice()));
        product.setDescription(product.getDescription());
        product.setCategory(Category.valueOf(productDto.getCategory().toUpperCase()));
        product.setImage(getBytes(multipartFile));
        Stock stock = new Stock();
        stock.setProduct(product);
        stock.setQuantity(Integer.valueOf(productDto.getQuantity()));
        product.setStock(stock);
        return product;
    }

    private byte[] getBytes(MultipartFile multipartFile){
        try {
            return multipartFile.getBytes();
        } catch (IOException e) {
            return new byte[0];
        }
    }

    public ProductDto mapProductDto (Product product){
        ProductDto productDto = new ProductDto();
        productDto.setName(product.getName());
        productDto.setPrice(product.getPrice().toString());
        productDto.setDescription(product.getDescription());
        productDto.setCategory(product.getCategory().name());
        productDto.setImage(Base64.encodeBase64String(product.getImage()));
        productDto.setQuantity(product.getStock().getQuantity().toString());
        productDto.setProductId(product.getProductId().toString());
        return productDto;
    }

    public List<ProductDto> mapProductDtoList(List<Product> productList){
        List<ProductDto> result = new ArrayList<>();
        for (Product product:productList){
            ProductDto productDto =  mapProductDto(product);
            result.add(productDto);
        }
        return result;
    }
}
