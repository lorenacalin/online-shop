package com.example.demo.mapper;

import com.example.demo.dto.OrderDto;
import com.example.demo.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper {
    @Autowired
    private ProductMapper productMapper;

    public OrderDto mapOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setStatus(order.getStatus().name());
        orderDto.setDate(order.getDateTime().toString());
        orderDto.setProductDtos(productMapper.mapProductDtoList(order.getProducts()));
        return orderDto;
    }
}
