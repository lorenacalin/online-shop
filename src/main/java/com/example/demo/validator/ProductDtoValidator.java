package com.example.demo.validator;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.enums.Category;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Service
public class ProductDtoValidator {
    @Autowired
    private ProductRepository productRepository;

    public void validate(ProductDto productDto, BindingResult bindingResult) {
        validateProductName(productDto, bindingResult);
        validateProductPrice(productDto, bindingResult);
        validateProductQuantity(productDto, bindingResult);
        validateProductCategory(productDto, bindingResult);
    }

    private void validateProductQuantity(ProductDto productDto, BindingResult bindingResult) {
        try {
            Integer quantity = Integer.valueOf(productDto.getQuantity());
            if (quantity <= 0) {
                FieldError fieldError = new FieldError("productDto", "quantity", "Quantity should be a positive number");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("productDto", "quantity", "Quantity should be a numerical value");
            bindingResult.addError(fieldError);
        }
    }

    private void validateProductCategory(ProductDto productDto, BindingResult bindingResult) {
        try {
            Category category = Category.valueOf(productDto.getCategory());

        } catch (IllegalArgumentException exception) {
            FieldError fieldError = new FieldError("productDto", "category", "Invalid category");
            bindingResult.addError(fieldError);
        }
    }

    private void validateProductName(ProductDto productDto, BindingResult bindingResult) {
        if (productDto.getName().length() == 0) {
            FieldError fieldError = new FieldError("productDto", "name", "Name cannot be empty!");
            bindingResult.addError(fieldError);
        }
    }

    private void validateProductPrice(ProductDto productDto, BindingResult bindingResult) {
        try {
            Integer quantity = Integer.valueOf(productDto.getQuantity());
            if (quantity <= 0) {
                FieldError fieldError = new FieldError("productDto", "quantity", "Quantity should be a positive number");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("productDto", "quantity", "Quantity should be a numerical value");
            bindingResult.addError(fieldError);
        }
    }
}
