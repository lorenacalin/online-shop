package com.example.demo.service;

import com.example.demo.dto.PriceSummaryDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityDto;
import com.example.demo.dto.QuantityPriceDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ShoppingCartService {
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    public boolean addProduct(String productId, QuantityDto quantityDto, String email) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(email);
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if (optionalProduct.isEmpty()) {
            return false;
        }
        Product product = optionalProduct.get();
        Integer quantity = Integer.valueOf(quantityDto.getQuantity());
        if (product.getStock().getQuantity() < quantity) {
            return false;
        }
        for (int index = 0; index < quantity; index++) {
            shoppingCart.addProduct(product);
        }
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    public Map<ProductDto, QuantityPriceDto> getShoppingCartContent(String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        List<Product> products = shoppingCart.getProducts();
        Map<ProductDto, QuantityPriceDto> productMap = new LinkedHashMap<>();
        for (Product product : products) {
            ProductDto productDto = productMapper.mapProductDto(product);
            if (productMap.containsKey(productDto)) {
                QuantityPriceDto quantityPriceDto = productMap.get(productDto);
                quantityPriceDto.setQuantity(quantityPriceDto.getQuantity() + 1);
                quantityPriceDto.setTotalPrice(quantityPriceDto.getTotalPrice() + Integer.parseInt(productDto.getPrice()));
            } else {
                QuantityPriceDto quantityPriceDto = new QuantityPriceDto();
                quantityPriceDto.setQuantity(1);
                quantityPriceDto.setTotalPrice(Integer.valueOf(productDto.getPrice()));
                productMap.put(productDto, quantityPriceDto);
            }
        }
        System.out.println(productMap);
        return productMap;
    }

    public PriceSummaryDto computePriceSummaryDto(Map<ProductDto, QuantityPriceDto> productMap) {
        Integer subtotal = productMap.values().stream()
                .map(p -> p.getTotalPrice())
                .mapToInt(Integer::intValue)
                .sum();
        Integer shippingTax = 25;
        Integer total = subtotal + shippingTax;
        return new PriceSummaryDto(subtotal, shippingTax, total);
    }
}
