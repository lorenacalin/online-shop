package com.example.demo.service;

import com.example.demo.dto.LoginDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.UserDto;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Product;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public void addUser(UserDto userDto){
        User user = userMapper.mapUser(userDto);
        userRepository.save(user);
    }

    public boolean login(LoginDto loginDto) {
        Optional<User> optionalUser = userRepository.findByEmail(loginDto.getEmail());
        if (optionalUser.isEmpty()){
            return false;
        }
        User user = optionalUser.get();
        if (!user.getPassword().equals(loginDto.getPassword())){
            return false;
        }
        return true;
    }
}
