package com.example.demo.model.enums;

public enum Role {
    ADMINISTRATOR,
    CUSTOMER
}
