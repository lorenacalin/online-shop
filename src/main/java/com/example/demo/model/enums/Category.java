package com.example.demo.model.enums;

public enum Category {
    LAPTOPS,
    MOBILE,
    PRINTING,
    TV,
    MONITORS
}
