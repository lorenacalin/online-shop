package com.example.demo.model.enums;

public enum Status {
    PLACED,
    IN_PROGRESS,
    DELIVERED
}