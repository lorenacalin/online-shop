package com.example.demo.model;

import com.example.demo.model.enums.Category;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer productId;
    private String name;
    private Integer price;
    private String description;
    @Lob
    private byte[] image;

    @Enumerated(value = EnumType.STRING)
    private Category category;

    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "productId"), inverseJoinColumns = @JoinColumn(name = "wishlistId"))
    private List<Wishlist> wishlists;

    @ManyToMany(mappedBy = "products", cascade = CascadeType.ALL)
    private List<ShoppingCart> shoppingCarts;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private Stock stock;

    public void decreaseQuantityByOne(){
        stock.decreaseQuantityByOne();
    }
}